import java.util.Scanner;

public class mq1 {
	public static void main(String[] args) {
		
		//store maze4
		char[][] maze = {{ 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},
			   		     { 'X', 'H', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'X'},
			             { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			             { 'X', 'D', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			             { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			             { 'X', ' ', ' ', ' ', ' ', ' ', ' ', 'X', ' ', 'E'},
			             { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			             { 'X', ' ', 'X', 'X', ' ', 'X', ' ', 'X', ' ', 'X'},
			             { 'X', 'K', 'X', 'X', ' ', ' ', ' ', ' ', ' ', 'X'},
			             { 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'}};

						
		
		
		//Game loop
		int hX, hY;
		Scanner s = new Scanner(System.in);
		char uc;
		boolean hasKey = false;
		boolean isGameOver = false;
		
		hX = 1;
		hY = 1;
		
		do {
			//print maze
			printMaze(maze);			
			//read input
			System.out.print("cmd>");
			uc = s.next().charAt(0);
			//update world
			switch(uc) {
			case 'w': //UP
				if (maze[hX-1][hY] == ' ') { //check for free space
					maze[hX-1][hY] = 'H';
					maze[hX][hY] = ' ';
					hX=hX-1;
					if(maze[hX-1][hY] == 'D') {
						printMaze(maze);
						System.out.println("GAME OVER");
						isGameOver = true;
					}
				}
				//System.out.println("up");
				break;
			case 'a': //LEFT
				if (maze[hX][hY-1] == ' ') { //check for free space
					maze[hX][hY-1] = 'H';
					maze[hX][hY] = ' ';
					hY=hY-1;
				}
				//System.out.println("left");
				break;
			case 'd': //RIGHT
				if (maze[hX][hY+1] == ' ') { //check for free space
					maze[hX][hY+1] = 'H';
					maze[hX][hY] = ' ';
					hY=hY+1;
				}
				if (maze[hX][hY+1] == 'E') { //check for key
					if(hasKey) {
						System.out.println("Great Job! You reached the exit!");
						isGameOver = true;	
					} else {
						System.out.println("You need a key to open this door.");
					}
				}
				//System.out.println("right");
				break;
			case 's': //DOWN
				if (maze[hX+1][hY] == ' ') { //check for free space
					maze[hX+1][hY] = 'H';
					maze[hX][hY] = ' ';
					hX=hX+1;	
					if(maze[hX+1][hY] == 'D') {
						printMaze(maze);
						System.out.println("GAME OVER");
						isGameOver = true;
					}
					break;
					
				}
				if(maze[hX+1][hY]=='K') {
					maze[hX+1][hY] = 'H';
					maze[hX][hY] = ' ';
					hX=hX+1;
					hasKey= true; 
				}
				//System.out.println("down");
				break;
			}
			
		} while (!(isGameOver)&&(uc != 'q'));
		
		System.out.println("Exiting!");
		s.close();
		}

		private static void printMaze(char[][] maze) {
			for(int i=0; i<maze.length;i++) 	{
				for(int j=0; j< maze[0].length ;j++) {
					System.out.print(maze[i][j]);
				}
				System.out.println();
			}
		}	
	}
